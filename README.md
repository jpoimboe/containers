# CKI Project Container Images

This repo contains the build scripts for the container images used by the CKI
Project. Each container image is built and hosted in GitLab CI using
[buildah](https://github.com/containers/buildah).

A thorough description on how the CKI project uses container images can be
found in the [public documentation].

Please only add new documentation here that is specific to the CKI container
image repository!

## Project Details

### Maintainers

[Iñaki Malerba](https://gitlab.com/inakimalerba)
[Michael Hofmann](https://gitlab.com/mh21)

### Design principles

- Only container image build files belong in the `builds` directory. Any
  additional content, such as certificates, repositories, or configuration
  files must be in the `files` directory.
- Keep container images small by cleaning up after yourself within the
  container. Watch out for applications that leave behind lots of cached data.

## Container image include files

The container image build files are split into multiple include files to create
some orthogonality and reduce duplication. This is a work in progress.

In each container image build file, exactly one of the setup include files
should be used via the first `#include` directive:

- `setup-minimal`: Common setup steps that should be included in all
  Fedora-based CKI container images. This is mainly concerned with setting up
  common dnf configuration, certificates and environment variables and
  installing a core Python 3 stack. This include file is not used by the
  internal container images. It includes the `envvars` and `certs` include
  files.
- `setup-pipeline`: Common setup steps that should be included in all
  Fedora-based images used in the pipeline. This includes the `setup-minimal`
  include file, and indirectly the `envvars` and `certs` include files.
- `setup-builder-internal`: Common setup steps that should be included in all
  non-Fedora builder images. This includes the `envvars` and `certs` include
  files.

All setup include files include the following files:

- `certs`: add Red Hat and CKI SSL certificates to the CA trust store
- `envvars`: configure common environment variables

Configuration of pipeline container images is split across the following files:

- `pipeline`: Common steps for all pipeline images.
- `builder-all`: Common steps for all pipeline builder images. This should
  include the `pipeline` include file, but this is currently not the case.
- `builder`: Common steps for the Fedora-based pipeline builder images. This
  includes the `builder-all` include file.
- `builder-internal`: Common steps for the non-Fedora pipeline builder images.
  This includes the `builder-all` include file.

Each container image build file should end with an `#include` directive for the
`cleanup` include file which takes care of common cleanup tasks such as
removing caches from dnf and pip.

The `setup-base` and `python-requirements` include files should only be used
for building application container images, i.e. not in one of the container
image repositories.

### Container image for ELN

The images are created from an official base image, usually that image is
`registry.fedoraproject.org/fedora`, but for [ELN] there is no official image
yet, so TEMPORARILY another helper `build_eln_from_scratch.sh` is used, to
create that base image.

The helper is called by `cki_build_image.sh` when the `IMAGE_NAME` is
`builder-eln`, so the builder can be generated in the same manner as
the rest.

For creating the `builder-eln` locally, the following can be used:

```shell
podman run \
    --rm \
    -e IMAGE_NAME=builder-eln \
    --privileged \
    -w /code \
    -v .:/code \
    -v ~/.local/share/containers:/var/lib/containers \
    registry.gitlab.com/cki-project/containers/buildah:latest \
    cki_build_image.sh
```

For creating just the base image (for developing of debugging purposes),
something similar can be used:

```shell
podman run \
    --rm \
    --privileged \
    -w /code \
    -v .:/code \
    -v ~/.local/share/containers:/var/lib/containers \
    registry.gitlab.com/cki-project/containers/buildah:latest \
    build_eln_from_scratch.sh
```

That base image can be generated for different architectures (`amd64`,
`arm64`, `ppc64le` and `s390x`). By default, the script generates the
image for `amd64`, but it can be changed by passing the enviroment variable
`IMAGE_ARCH` with a different architecture name. For example, the following
would generate a `builder-eln` for `ppc64le`:

```shell
podman run \
    --rm \
    -e IMAGE_ARCH=ppc64le \
    --privileged \
    -w /code \
    -v .:/code \
    -v ~/.local/share/containers:/var/lib/containers \
    registry.gitlab.com/cki-project/containers/buildah:latest \
    build_eln_from_scratch.sh
```

**NOTE:** This is a temporary solution and it can be removed once the upstream
images are available. This work is tracked in <https://github.com/fedora-eln/eln/issues/16>.

More details about how the helper `build_eln_from_scratch.sh` works can be
found at [ELN_from_scratch.md](ELN_from_scratch.md).

## Native builders

The repository has native builders attached with tags following the pattern
`container-build-runner-{kernel-arch}`. These builders also have access to
RHEL7 and RHEL8 RPM repositories.

## Current container images

This repository currently provides the following container images:

### base

This container image is the preferred base image of derived container images
and only includes a minimal Python 3 and pip environment.

### buildah

This multi-arch container image can run buildah to build container images.

### python

This container image is the preferred image for all CI/CD pipelines in GitLab.
Next to Python 3, it includes everything that is needed to run tests for CKI
projects.

### builder-fedora

This container image can compile upstream Linux kernels and it also includes
extra packages for Python.

### builder-rawhide

Similar to `builder-fedora`, but based on a Rawhide environment.

### builder-eln

Similar to `builder-fedora`, but based on a [ELN] environment.

### builder-rhel7

This container image can compile RHEL 7 Linux kernels.

### builder-rhel8

Similar to `builder-rhel7`, but for RHEL 8.

### builder-rhel9

Similar to `builder-rhel7`, but for RHEL 9.

### doc-helpers

Container image with [Node.js], [Hugo], [Muffet] and [markdownlint] installed
to use it at the Gitlab's CI/CD pipelines for linting documentation files,
check broken links or build web sites with Hugo.

## Mirrored images

The container image registry connected to this repository also hosts a mirror of
various container images from Docker Hub. For details, consult the [public
documentation].

[public documentation]: https://cki-project.org/docs/hacking/contributing/container-images
[gitlab-cki.yml]: includes/gitlab-cki.yml
[ELN]: https://docs.fedoraproject.org/en-US/eln/
[Node.js]: https://nodejs.org
[Hugo]: https://gohugo.io
[Muffet]: https://github.com/raviqqe/muffet
[markdownlint]: https://github.com/DavidAnson/markdownlint
