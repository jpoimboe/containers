# Building ELN base image from Scratch

This file tries to explain how the helper `build_eln_from_scratch.sh` works
and some of the particularities of the base image generated with it.

Keep in mind that this helper is temporary and it'll be obsolete once the
Fedora Project generates ELN base images for all the architectures required
by CKI.

## Building a minimal image with Buildah and dnf

[Buildah] is a powerful tool that allow you to create containers, mount its
filesystem somewhere and manipulate it, to later create a container image.

With this tool and the parameter `--installroot=` from the package management
tool `dnf` it is easy to generate minimal container images based on any
distribution that uses `dnf` or just a minimal image (maybe a image with
`busybox`).

This is exaplained in more detail in the Buildah documentation at
[building a container from scratch](https://github.com/containers/buildah/blob/master/docs/tutorials/01-intro.md#building-a-container-from-scratch)

This could be a basic example:

```bash
#!/bin/sh

newcontainer=$(buildah from scratch)
scratchmnt=$(buildah mount "${newcontainer}")

dnf install --installroot "${scratchmnt}" --releasever 33 -y busybox \
            --setopt install_weak_deps=false --repo fedora

buildah config --entrypoint '[ "/sbin/busybox", "sh"]' "${newcontainer}"

rm -fr "${scratchmnt}"/var/cache/*

buildah unmount "${newcontainer}"
buildah commit "${newcontainer}" "my_busybox"
buildah rm "${newcontainer}"
```

It will produce a very small container image called `my_busybox` which
just contains the program `busybox`, without any package manager or
any other system dependencies:

```shell
$ podman images
REPOSITORY                    TAG         IMAGE ID      CREATED        SIZE
localhost/my_busybox          latest      b28469b6f752  7 seconds ago  1.83 MB
```

Be aware that buildah uses the overlay driver to be able to mount
and modify the container filesystem. This can't be done as a normal user.

In order to run the script you need to become root or (better) run with
`buildah unshare`, so it runs in modified user namespace. It can be done like
this:

```shell
$ buildah unshare ./the_script.sh
```

**NOTE:** When it runs inside the container or at Gitlab-CI, it runs as a root
user, so it doesn't need the `buildah unshare`. This is just to test it locally
as a normal user.

## ELN configuration

The list of packages, some extra configurations and post-installation pruning
is copied and adapted (where needed) from the [Kickstart] file of the
original image from upstream (the [kickstart file]).

But other things have been added:

### Repositories and dnf options

#### Installing packages

`dnf` needs to know the repositories for this fedora version, so they're been
passed as arguments with `--repofrompath`. That way it's possible to add
non-standard repositories by passing a name and URL.

But previously, we need to disable all the repositories, otherwise `dnf` will
use the host's repositories. The host, in this case, is the system running the
script. It could be a physical machine or a container. Either way, `dnf` will
use the host configurations.

That's why the argument `--disableplugin=versionlock` is passed. The builder
container (the one used at Gitlab-CI for building the images) has this plugin
installed and some packages pinned, so `dnf` can't upgrade them.

The problem is that ELN might have higher versions for those packages than the
builder container, so it'll prevent to install that package and this can lead
to a chain of broken dependencies.

Actually, this was the case for the package `shadow-utils`, which is pinned in
the builder container.

Another flag needed to be able to use the ELN repositories is `--nogpgcheck`.
Later the packages with the repos and the keys will be imported, but at this
stage `dnf` doesn't have the gpg keys and would fail without this flag.

Lastly, another important argument is `--forcearch` with the actual architecture
of the system that is going to be installed. Otherwhise, it tries to install
the packages for the architecture of the host.

Notice that the URLs for the `--repofrompath` also has the architecture in it,
so it can find the right packages.

### Reconfiguring repositories

After the installation of the base system. any extra repositories installed inside
the container need to be disabled.

with the ELN repositories.

This is done with `dnf config-manager --disable "*rawhide*" "*cisco*"`, but
it's ran using `chroot`, so it runs inside the container.

Also, as the repository `eln-everything` is not installed by the package
`fedora-repos-eln` and it'll be needed later to install some packages, the repo
is added "*manually*" to the container's repo file
`/etc/yum.repos.d/fedora-eln.repo`.

### Architectures

To be able to create container images for the different architectures supported
by CKI (`amd64`, `arm64`, `ppc64le` and `s390x`), the script checks for an
environment variable `IMAGE_ARCH`. If the variable exist, it'll create the
image for that architecture, othrwise it'll use the default, `amd64`.

It's important to be aware of the difference between the name of the
architectures for the system (`uname -m`) and for the packages. Sometimes
thay are the same, but sometimes they aren't.

Let's see the different cases in the architecture names:

| System | Packages |
| :----: | :------: |
| amd64  | x86_64   |
| arm64  | aarch64  |
| pc64le | ppc64le  |
| s390x  | s390x    |

### Configure the container

Once the container is ready and is being cleaned from unnecessary files, the
script add some metadata to the container, so the final image has that basic
metadata.

This is done via the command `buildah config`.

Most of the metadata is the same as for the upstream version, the rest is just
to know that the version is generated by CKI.

[Buildah]: https://github.com/containers/buildah
[Kickstart]: https://pykickstart.readthedocs.io/en/latest/kickstart-docs.html
[kickstart file]: https://pagure.io/fedora-kickstarts/blob/master/f/fedora-eln-container-base.ks
